This extension contains a library of DNA modifications (see
https://gitlab.com/KomBioMol/pmx-ff-mutdna/-/blob/master/amber99sb-star-ildn-bsc1-mut-uptodate.ff/new_dna_residues.dat for a full list)
that can be easily introduced into alchemical free energy calculations, just as
in the regular PMX workflow (http://pmx.mpibpc.mpg.de/).

The .ff directory is intended to work with PMX (https://github.com/deGrootLab/pmx) 
in its python3-compatible version. It will be integrated into the tool soon, once
it is fully developed and released.

If you want to use this library before Py3-PMX is released, contact the developer
at milafternoon@gmail.com.